<?php

$router = app('Dingo\Api\Routing\Router');

$router->version('v1', function ($route) {
    // Auth
    $route->post('register', 'App\Http\Controllers\Api\RegisterController@register');
    $route->post('login', 'App\Http\Controllers\Api\LoginController@login');
    $route->post('logout', 'App\Http\Controllers\Api\LoginController@logout');
});
