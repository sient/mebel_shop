<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;

Breadcrumbs::for('admin.index', function ($trait) {
    $trait->push('Главная', route('admin.index'));
});
// Users
Breadcrumbs::for('admin.users.index', function ($trait) {
   $trait->parent('admin.index');
   $trait->push('Список пользователей', route('admin.users.index'));
});

Breadcrumbs::for('admin.users.edit', function ($trait) {
    $trait->parent('admin.users.index');
    $trait->push('Редактирование пользователя', '');
});

// Categories
Breadcrumbs::for('admin.categories.index', function ($trait) {
    $trait->parent('admin.index');
    $trait->push('Каталог товаров', route('admin.categories.index'));
});

Breadcrumbs::for('admin.categories.create', function ($trait) {
    $trait->parent('admin.categories.index');
    $trait->push('Добавление категории', route('admin.categories.create'));
});

Breadcrumbs::for('admin.categories.show', function ($trait, $title) {
    $trait->parent('admin.categories.index');
    $trait->push($title, route('admin.categories.create'));
});

Breadcrumbs::for('admin.categories.edit', function ($trait) {
    $trait->parent('admin.categories.index');
    $trait->push('Редактирование категории', '');
});

// Products
Breadcrumbs::for('admin.products.create', function ($trait) {
    $trait->parent('admin.categories.index');
    $trait->push('Добавление товара', '');
});

Breadcrumbs::for('admin.products.edit', function ($trait, $category) {
    $trait->parent('admin.categories.index');
    $trait->push($category->name, route('admin.categories.show', $category->id));
    $trait->push('Редактирование товара', '');
});

// Category attributes
Breadcrumbs::for('admin.categories.attributes.index', function ($trait, $category) {
    $trait->parent('admin.categories.index');
    $trait->push('Атрибуты для категории ' . $category->name, '');
});

Breadcrumbs::for('admin.categories.attributes.create', function ($trait, $category) {
    $trait->parent('admin.categories.index');
    $trait->push('Атрибуты для категории ' . $category->name, route('admin.categories.attributes.index', $category->id));
    $trait->push('Добавление аттрибута', '');
});

Breadcrumbs::for('admin.categories.attributes.edit', function ($trait, $category) {
    $trait->parent('admin.categories.index');
    $trait->push('Атрибуты для категории ' . $category->name, route('admin.categories.attributes.index', $category->id));
    $trait->push('Редактирование аттрибута', '');
});

// Product attributes
Breadcrumbs::for('admin.products.attributes.index', function ($trait, $category) {
    $trait->parent('admin.categories.index');
    $trait->push($category->name, route('admin.categories.show', $category->id));
    $trait->push('Атрибуты товара', '');
});
