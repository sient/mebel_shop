<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Web'], function () {
    Route::get('', 'HomeController@index')->name('home');
    Route::post('', 'LoginController@login');
    Route::get('logout', 'LoginController@logout')->name('logout');


    // Admin
    Route::group(
        [
            'middleware'    => 'web.admin',
            'prefix'        => 'admin',
            'namespace'     => 'Admin',
            'as'            => 'admin.'
        ],
        function ()
        {
            Route::get('', 'HomeController@index')->name('index');

            // Users
            Route::group(
                [
                    'prefix'    => 'users',
                    'as'        => 'users.'
                ],
                function () {
                    Route::get('', 'UserController@index')->name('index');
                    Route::get('edit/{id}', 'UserController@edit')->name('edit');
                    Route::post('edit/{id}', 'UserController@update');
                }
            );

            // Categories
            Route::group(
                [
                    'prefix'    => 'categories',
                    'as'        => 'categories.'
                ],
                function () {
                    Route::get('', 'CategoryController@index')->name('index');
                    Route::get('show/{id}', 'CategoryController@show')->name('show');
                    Route::get('create', 'CategoryController@create')->name('create');
                    Route::post('create', 'CategoryController@store');
                    Route::get('edit/{id}', 'CategoryController@edit')->name('edit');
                    Route::post('edit/{id}', 'CategoryController@update');

                    // Attributes
                    Route::group(
                        [
                            'prefix'    => 'attributes/{category_id}',
                            'as'        => 'attributes.'
                        ],
                        function () {
                            Route::get('', 'CategoryAttributeController@index')->name('index');
                            Route::get('create', 'CategoryAttributeController@create')->name('create');
                            Route::post('create', 'CategoryAttributeController@store');
                            Route::get('edit/{id}', 'CategoryAttributeController@edit')->name('edit');
                            Route::post('edit/{id}', 'CategoryAttributeController@update');
                        }
                    );
                }
            );

            // Products
            Route::group(
                [
                    'prefix'    => 'products',
                    'as'        => 'products.'
                ],
                function () {
                    Route::get('create', 'ProductController@create')->name('create');
                    Route::post('create', 'ProductController@store');
                    Route::get('edit/{id}', 'ProductController@edit')->name('edit');
                    Route::post('edit/{id}', 'ProductController@update');
                    Route::get('delete-image/{id}', 'ProductController@deleteImage')->name('delete-image');

                    // Attributes
                    Route::group(
                        [
                            'prefix'    => 'attributes/{product_id}',
                            'as'        => 'attributes.'
                        ],
                        function () {
                            Route::get('', 'ProductAttributeController@index')->name('index');
                            Route::post('', 'ProductAttributeController@save');
                        }
                    );
                }
            );
        }
    );
});
