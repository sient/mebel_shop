<!doctype html>
<html class="no-js " lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>Авторизация</title>

    <link rel="icon" href="/favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin-assets/css/style.min.css">
</head>

<body class="theme-blush">

<div class="authentication">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-12">
                <form class="card auth_form" method="POST">
                    @csrf

                    <div class="header">
                        <img class="logo" src="/admin-assets/images/logo.svg" alt="">
                        <h5>Авторизация</h5>
                    </div>
                    <div class="body">
                        <div class="form-group mb-3">
                            @error('email')
                            <label class="error" for="email">{{ $message }}</label>
                            @enderror
                            <input type="email" name="email" id="email" class="form-control @error('email') form-control-danger @enderror" value="{{ old('email') }}" placeholder="E-mail" required>
                        </div>
                        <div class="form-group mb-3">
                            @error('password')
                            <label class="error" for="password">{{ $message }}</label>
                            @enderror
                            <input type="password" id="password" name="password" class="form-control @error('email') form-control-danger @enderror" placeholder="Пароль">
                        </div>
                        <div class="checkbox">
                            <input id="remember_me" type="checkbox">
                            <label for="remember_me">Запомнить</label>
                        </div>
                        <button type="sumbit" class="btn btn-primary btn-block waves-effect waves-light">ВОЙТИ</button>
                    </div>
                </form>
            </div>
            <div class="col-lg-8 col-sm-12">
                <div class="card">
                    <img src="/admin-assets/images/signin.svg" alt="Sign In"/>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="/admin-assets/bundles/libscripts.bundle.js"></script>
<script src="/admin-assets/bundles/vendorscripts.bundle.js"></script>

</body>
</html>
