@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                </div>
                <div class="body">
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <label for="name">Название</label>
                        <div class="form-group">
                            @error('name')
                            <label class="error" for="name">{{ $message }}</label>
                            @enderror
                            <input type="text" id="name" value="{{ old('name') }}" name="name" class="form-control" required>
                        </div>

                        <label for="icon">Иконка</label>
                        <div class="form-group">
                            @error('icon')
                            <label class="error" for="icon">{{ $message }}</label>
                            @enderror
                            <input type="file" name="icon" id="icon" class="dropify" data-allowed-file-extensions="png jpg jpeg" required>
                        </div>

                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Добавить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
