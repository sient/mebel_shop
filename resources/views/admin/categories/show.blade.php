@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                    <br>
                    <h2><a href="{{ route('admin.products.create') }}" class="btn btn-light">Добавить товар</a></h2>
                </div>
            </div>
        </div>
        @foreach($products as $product)
            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6">
                <div class="card">
                    <div class="body product_item">
                        <img src="{{ $product->image->image }}" alt="Product" class="img-fluid cp_img" />
                        <div class="product_details">
                            <a href="{{ route('admin.products.edit', $product->id) }}">{{ $product->name }}</a>
                            <ul class="product_price list-unstyled">
                                <li class="new_price">{{ $product->price }}</li>
                            </ul>
                        </div>
                        <div class="action">
                            <a href="{{ route('admin.products.attributes.index', $product->id) }}" class="btn btn-default waves-effect">
                                <i class="zmdi zmdi-ruler"></i>
                            </a>
                            <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-info waves-effect">
                                <i class="zmdi zmdi-edit"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

        {{ $products->links() }}
    </div>
@endsection
