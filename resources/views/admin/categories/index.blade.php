@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                    <br>
                    <h2><a href="{{ route('admin.categories.create') }}" class="btn btn-light">Добавить категорию</a></h2>
                </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th class="text-left">#</th>
                                <th>Название</th>
                                <th>Иконка</th>
                                <th class="text-right">
                                    <i class="zmdi zmdi-settings"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($categories as $category)
                                <tr>
                                    <td class="text-left">{{ $category->id }}</td>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        <img src="{{ $category->icon }}" alt="" width="100">
                                    </td>
                                    <td class="text-right">
                                        <a href="{{ route('admin.categories.attributes.index', $category->id) }}" class="btn btn-default btn-sm">
                                            <i class="zmdi zmdi-ruler"></i>
                                        </a>
                                        <a href="{{ route('admin.categories.show', $category->id) }}" class="btn btn-info btn-sm">
                                            <i class="zmdi zmdi-labels"></i>
                                        </a>
                                        <a href="{{ route('admin.categories.edit', $category->id) }}" class="btn btn-primary btn-sm">
                                            <i class="zmdi zmdi-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>

                    {{ $categories->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
