@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                </div>
                <div class="body">
                    <form method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <label for="name">Название</label>
                                <div class="form-group">
                                    @error('name')
                                    <label class="error" for="name">{{ $message }}</label>
                                    @enderror
                                    <input type="text" id="name" value="{{ old('name', $product->name) }}" name="name" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="category_id">Категория</label>
                                <div class="form-group">
                                    @error('category_id')
                                    <label class="error" for="category_id">{{ $message }}</label>
                                    @enderror
                                    <select name="category_id" id="category_id" class="form-control show-tick ms select2">
                                        @foreach($categories as $category)
                                            <option
                                                value="{{ $category->id }}"
                                                {{ $product->category_id == $category->id ? 'selected' : '' }}
                                            >
                                                {{ $category->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="price">Цена</label>
                                @error('price')
                                <label class="error" for="price">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="number" step="0.01" id="price" value="{{ old('price', $product->price) }}" min="0" name="price" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <label for="article">Артикул</label>
                                @error('article')
                                <label class="error" for="article">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="text" id="article" value="{{ old('article', $product->article) }}" name="article" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="brand">Бренд</label>
                                @error('brand')
                                <label class="error" for="brand">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="text" id="brand" value="{{ old('brand', $product->brand) }}" name="brand" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label for="manufacturer">Производитель</label>
                                @error('manufacturer')
                                <label class="error" for="article">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="text" id="manufacturer" value="{{ old('manufacturer', $product->manufacturer) }}" name="manufacturer" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label for="guarantee">Гарантия (месяцев)</label>
                                @error('guarantee')
                                <label class="error" for="guarantee">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="number" step="1" id="guarantee" value="{{ old('guarantee', $product->guarantee) }}" name="guarantee" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="life_time">Срок службы (лет)</label>
                                @error('life_time')
                                <label class="error" for="life_time">{{ $message }}</label>
                                @enderror
                                <div class="form-group">
                                    <input type="number" step="1" id="life_time" value="{{ old('life_time', $product->life_time) }}" name="life_time" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="images">Изображения</label>
                                <div class="form-group">
                                    @error('images')
                                    <label class="error" for="images">{{ $message }}</label>
                                    @enderror
                                    <input type="file" name="images[]" id="images" class="dropify" data-allowed-file-extensions="png jpg jpeg" multiple>
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Сохранить</button>
                    </form>

                    <section class="file_manager">
                        <div class="body_scroll">
                            <div class="container-fluid">
                                <div class="row clearfix">
                                    <div class="col-lg-12">
                                        <div class="card">
                                            <div class="header">
                                                <h2><strong>Изображения</strong></h2>
                                            </div>
                                            <div class="tab-content">
                                                <div class="tab-pane active">
                                                    <div class="row clearfix">
                                                        @foreach($product->images as $image)
                                                            <div class="col-lg-3 col-md-4 col-sm-12">
                                                                <div class="card">
                                                                    <a href="{{ route('admin.products.delete-image', $image->id) }}" class="file">
                                                                        <div class="hover">
                                                                            <button type="button" class="btn btn-icon btn-icon-mini btn-round btn-danger">
                                                                                <i class="zmdi zmdi-delete"></i>
                                                                            </button>
                                                                        </div>
                                                                        <div class="image">
                                                                            <img src="{{ $image->image }}" width="300" alt="img" class="img-fluid">
                                                                        </div>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection
