<ul class="breadcrumb">
    @foreach(Breadcrumbs::generate() as $breadcrumb)
        @if(!$loop->last)
            <li class="breadcrumb-item">
                <a href="{{ $breadcrumb->url }}">@if($loop->first)<i class="zmdi zmdi-home"></i> @endif{{ $breadcrumb->title }}</a>
            </li>
        @else
            <li class="breadcrumb-item active">{{ $breadcrumb->title }}</li>
        @endif
    @endforeach
</ul>
