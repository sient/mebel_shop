<html class="no-js " lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>{{ $title ?? '' ? ($title . ' - ') : '' }} Mebel.shop</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon"> <!-- Favicon-->
    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/admin-assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
    <link rel="stylesheet" href="/admin-assets/plugins/charts-c3/plugin.css"/>

    <link rel="stylesheet" href="/admin-assets/plugins/morrisjs/morris.min.css" />
    <link rel="stylesheet" href="/admin-assets/plugins/multi-select/css/multi-select.css">
    <link rel="stylesheet" href="/admin-assets/plugins/dropify/css/dropify.min.css">
    <link rel="stylesheet" href="/admin-assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

    <!-- Custom Css -->
    <link rel="stylesheet" href="/admin-assets/css/style.min.css">

    @yield('css')

</head>

<body class="theme-blush right_icon_toggle">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30">
            <img class="zmdi-hc-spin" src="/admin-assets/images/loader.svg" width="48" height="48" alt="Aero">
        </div>
        <p>Загрузка...</p>
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="navbar-brand">
        <button class="btn-menu ls-toggle-btn" type="button"><i class="zmdi zmdi-menu"></i></button>
        <a href="{{ route('admin.index') }}">
            <img src="/admin-assets/images/logo.svg" width="25" alt="Сайт юридических услуг">
            <span class="m-l-10">Админ-панель</span>
        </a>
    </div>
    <div class="menu">
        <ul class="list">
            <li>
                <div class="user-info">
                    <a class="image" href="#"><img src="/admin-assets/images/profile_av.jpg" alt="User"></a>
                    <div class="detail">
                        <h4>{{ Auth()->user()->name }}</h4>
                    </div>
                </div>
            </li>

            <li class="{{ request()->is('admin') ? 'active open' : '' }}">
                <a href="{{ route('admin.index') }}">
                    <i class="zmdi zmdi-home"></i>
                    <span>Главная</span>
                </a>
            </li>

            <li class="{{ request()->is('admin/users*') ? 'active open' : '' }}">
                <a href="{{ route('admin.users.index') }}">
                    <i class="zmdi zmdi-account"></i>
                    <span>Пользователи</span>
                </a>
            </li>
            <li class="{{ request()->is('admin/categories*') || request()->is('admin/product*') ? 'active open' : '' }}">
                <a href="{{ route('admin.categories.index') }}">
                    <i class="zmdi zmdi-grid"></i>
                    <span>Каталог товаров</span>
                </a>
            </li>
        </ul>
    </div>
</aside>

<!-- Main Content -->

<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>{{ $title ?? 'Главная' }}</h2>
                @include('admin.layouts.breadcrumbs')
                <button class="btn btn-primary btn-icon mobile_menu" type="button"><i class="zmdi zmdi-sort-amount-desc"></i></button>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @yield('content')
    </div>
</section>


<!-- Jquery Core Js -->
<script src="/admin-assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js ( jquery.v3.2.1, Bootstrap4 js) -->
<script src="/admin-assets/bundles/vendorscripts.bundle.js"></script> <!-- slimscroll, waves Scripts Plugin Js -->

<script src="/admin-assets/bundles/jvectormap.bundle.js"></script> <!-- JVectorMap Plugin Js -->
<script src="/admin-assets/bundles/sparkline.bundle.js"></script> <!-- Sparkline Plugin Js -->
<script src="/admin-assets/bundles/c3.bundle.js"></script>

<script src="/admin-assets/bundles/mainscripts.bundle.js"></script>
<script src="/admin-assets/js/pages/index.js"></script>
<script src="/admin-assets/plugins/select2/select2.min.js"></script> <!-- Select2 Js -->

<script src="/admin-assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->

<script src="/admin-assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

<script src="/admin-assets/plugins/dropify/js/dropify.min.js"></script>
<script src="/admin-assets/js/pages/forms/dropify.js"></script>

@if(session()->has('error'))
    <script>
        $.notify({
                message: '{{ session() ->get('error') }}'
            },
            {
                type: 'alert-danger',
                allow_dismiss: true,
                newest_on_top: true,
                timer: 100000,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                animate: {
                    enter: '',
                    exit: ''
                },
                template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (true ? "" : "") + '" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
            });
    </script>
@elseif(session()->has('success'))
    <script>
        $.notify({
                message: '{{ session() ->get('success') }}'
            },
            {
                type: 'alert-success',
                allow_dismiss: true,
                newest_on_top: true,
                timer: 100000,
                placement: {
                    from: 'top',
                    align: 'center'
                },
                animate: {
                    enter: '',
                    exit: ''
                },
                template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (true ? "" : "") + '" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title">{1}</span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
            });
    </script>
@endif

@yield('js')

</body>
</html>
