@extends('admin.layouts.app')

@section('css')
    <link rel="stylesheet" href="/assets/plugins/multi-select/css/multi-select.css">
    <link href="/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
@endsection

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                </div>
                <div class="body">
                    <form method="POST">
                        @csrf

                        <div class="row clearfix">

                            <div class="col-md-12">
                                <label for="email">Email</label>
                                <div class="form-group">
                                    <input type="email" id="email" value="{{ $user->email }}" class="form-control" disabled>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <label for="name">Имя</label>
                                <div class="form-group">
                                    <input type="text" name="name" id="name" value="{{ old('name', $user->name) }}" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="role">Роль</label>
                                <div class="form-group">
                                    <select name="role" id="role" class="form-control show-tick ms select2">
                                        @foreach($roles as $key => $value)
                                            <option value="{{ $key }}" {{ $key == $user->role ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js -->
    <script src="/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <script src="/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js -->
    <script src="/assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js -->
    <script src="/assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js -->

    <script>
        $('.datetimepicker').bootstrapMaterialDatePicker({
            format: 'YYYY-MM-DD',
            clearButton: true,
            weekStart: 1,
            time: false
        });
    </script>
@endsection
