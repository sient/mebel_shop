@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                </div>
                <div class="body">
                    <form method="POST">
                        @csrf
                        <div class="row clearfix">
                            @foreach($attributes as $attribute)
                                <div class="col-md-12">
                                    <label for="{{ $attribute->id }}">{{ $attribute->name }}{{ $attribute->postfix ? ', ' . $attribute->postfix : '' }}</label>
                                    <div class="form-group">
                                        @if($attribute->getType() == 'string' || $attribute->getType() == 'integer')
                                            <input
                                                @if($attribute->getType() == 'string')
                                                    type="text"
                                                @else
                                                    type="number"
                                                @endif
                                                id="{{ $attribute->id }}"
                                                @if($attribute->value)
                                                    value="{{ $attribute->value->value }}"
                                                @endif
                                                name="{{ $attribute->id }}"
                                                class="form-control"
                                            >
                                        @elseif($attribute->getType() == 'bool')
                                            <select name="{{ $attribute->id }}" id="{{ $attribute->id }}" class="form-control show-tick ms select2">
                                                <option
                                                    value=""
                                                    @if(!$attribute->value)
                                                        selected
                                                    @endif
                                                >Не выбрано</option>
                                                <option
                                                    value="1"
                                                    @if($attribute->value)
                                                        {{ $attribute->value->value == 1 ? 'selected' : '' }}
                                                    @endif
                                                >Да</option>
                                                <option
                                                    value="0"
                                                    @if($attribute->value)
                                                        {{ $attribute->value->value == 0 ? 'selected' : '' }}
                                                    @endif
                                                >Нет</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
