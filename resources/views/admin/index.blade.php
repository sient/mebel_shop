@extends('admin.layouts.app')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
                <div class="card">
                    <div class="body">
                        <input type="text" class="knob" value="{{ $users['percent'] }}" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#00adef" readonly>
                        <p>Пользователей</p>
                        <div class="d-flex bd-highlight text-center mt-4">
                            <div class="flex-fill bd-highlight">
                                <small class="text-muted">Сегодня</small>
                                <h5 class="mb-0">{{ $users['today'] }}</h5>
                            </div>
                            <div class="flex-fill bd-highlight">
                                <small class="text-muted">Всего</small>
                                <h5 class="mb-0">{{ $users['total'] }}</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 col-6 text-center">
                <div class="card">
                    <div class="body">
                        <input type="text" class="knob" value="81" data-linecap="round" data-width="100" data-height="100" data-thickness="0.08" data-fgColor="#ee2558" readonly>
                        <p>Заказов</p>
                        <div class="d-flex bd-highlight text-center mt-4">
                            <div class="flex-fill bd-highlight">
                                <small class="text-muted">Сегодня</small>
                                <h5 class="mb-0">123</h5>
                            </div>
                            <div class="flex-fill bd-highlight">
                                <small class="text-muted">Всего</small>
                                <h5 class="mb-0">1234</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card product-report">
                    <div class="header">
                        <h2><strong>Annual</strong> Report</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="icon xl-amber m-b-15"><i class="zmdi zmdi-chart-donut"></i></div>
                                <div class="col-in">
                                    <small class="text-muted mt-0">Sales Report</small>
                                    <h4 class="mt-0">$4,516</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="icon xl-blue m-b-15"><i class="zmdi zmdi-chart"></i></div>
                                <div class="col-in">
                                    <small class="text-muted mt-0">Annual Revenue</small>
                                    <h4 class="mt-0">$6,481</h4>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="icon xl-purple m-b-15"><i class="zmdi zmdi-card"></i></div>
                                <div class="col-in">
                                    <small class="text-muted mt-0">Total Profit</small>
                                    <h4 class="mt-0">$3,915</h4>
                                </div>
                            </div>
                        </div>
                        <div id="area_chart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="/admin-assets/bundles/morrisscripts.bundle.js"></script> <!-- Morris Plugin Js -->
    <script src="/admin-assets/bundles/knob.bundle.js"></script>

    <script src="/admin-assets/js/pages/ecommerce.js"></script>
    <script src="/admin-assets/js/pages/charts/jquery-knob.min.js"></script>
@endsection
