@extends('admin.layouts.app')

@section('content')
    <div class="row clearfix">
        <div class="col-md-12">
            <div class="card">
                <div class="header">
                    <h2><strong>{{ $title }}</strong></h2>
                </div>
                <div class="body">
                    <form method="POST">
                        @csrf
                        <div class="row clearfix">
                            <div class="col-md-12">
                                <label for="name">Название</label>
                                <div class="form-group">
                                    @error('name')
                                    <label class="error" for="name">{{ $message }}</label>
                                    @enderror
                                    <input type="text" id="name" value="{{ old('name', $attribute->name) }}" name="name" class="form-control" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="type">Тип</label>
                                <div class="form-group">
                                    @error('type')
                                    <label class="error" for="type">{{ $message }}</label>
                                    @enderror
                                    <select name="type" id="type" class="form-control show-tick ms select2">
                                        @foreach($types as $key => $value)
                                            <option value="{{ $key }}" {{ $key == $attribute->type ? 'selected' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <label for="postfix">Постфикс (Пример: см, мм, кг, шт)</label>
                                <div class="form-group">
                                    @error('postfix')
                                    <label class="error" for="postfix">{{ $message }}</label>
                                    @enderror
                                    <input type="text" id="postfix" value="{{ old('postfix', $attribute->postfix) }}" name="postfix" class="form-control">
                                </div>
                            </div>
                        </div>

                        <button type="submit" class="btn btn-raised btn-primary btn-round waves-effect">Сохранить</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
