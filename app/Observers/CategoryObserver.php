<?php

namespace App\Observers;

use App\Http\Helpers\Uploader;
use App\Models\Category;

class CategoryObserver
{
    /**
     * @param Category $category
     */
    public function creating(Category $category)
    {
        $category->icon = Uploader::upload('categories/icons', $category->icon);
    }

    /**
     * Handle the Category "created" event.
     *
     * @param Category $category
     * @return void
     */
    public function created(Category $category)
    {
        //
    }

    public function updating(Category $category)
    {
        if ($category->isDirty('icon')) {
            $category->icon = Uploader::upload('categories/icons', $category->icon);
        }
    }

    /**
     * Handle the Category "updated" event.
     *
     * @param Category $category
     * @return void
     */
    public function updated(Category $category)
    {
        //
    }

    /**
     * Handle the Category "deleted" event.
     *
     * @param Category $category
     * @return void
     */
    public function deleted(Category $category)
    {
        //
    }

    /**
     * Handle the Category "restored" event.
     *
     * @param Category $category
     * @return void
     */
    public function restored(Category $category)
    {
        //
    }

    /**
     * Handle the Category "force deleted" event.
     *
     * @param Category $category
     * @return void
     */
    public function forceDeleted(Category $category)
    {
        //
    }
}
