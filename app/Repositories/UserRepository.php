<?php

namespace App\Repositories;

use App\Models\User as Model;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param int|null $perPage
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = ['id', 'name', 'email', 'role', 'created_at'];

        return $this
            ->startConditions()
            ->select($columns)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function firstForEdit(int $id): Model
    {
        return $this
            ->startConditions()
            ->find($id);
    }
}
