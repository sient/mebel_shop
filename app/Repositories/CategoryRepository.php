<?php

namespace App\Repositories;

use App\Models\Category as Model;

/**
 * Class CategoryRepository
 * @package App\Repositories
 */
class CategoryRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param int|null $perPage
     * @return mixed
     */
    public function getAllWithPaginate($perPage = null)
    {
        $columns = ['id', 'name', 'icon'];

        return $this
            ->startConditions()
            ->select($columns)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * @param int $id
     * @return Model
     */
    public function firstForEdit(int $id): Model
    {
        return $this
            ->startConditions()
            ->find($id);
    }

    /**
     * @return mixed
     */
    public function getForSelect()
    {
        $columns = ['id', 'name'];

        return $this
            ->startConditions()
            ->select($columns)
            ->toBase()
            ->get();
    }
}
