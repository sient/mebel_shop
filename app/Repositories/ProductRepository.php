<?php

namespace App\Repositories;

use App\Models\Product as Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class ProductRepository
 * @package App\Repositories
 */
class ProductRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param int $id
     * @return Builder|Builder[]|Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function firstForEdit(int $id)
    {
        return $this
            ->startConditions()
            ->with('images', 'category')
            ->find($id);
    }

    /**
     * @param int $category_id
     * @param null|int $perPage
     * @return mixed
     */
    public function getByCategoryId(int $category_id, $perPage = null)
    {
        $columns = ['id', 'category_id', 'name', 'price'];

        return $this
            ->startConditions()
            ->where('category_id', $category_id)
            ->select($columns)
            ->with('image')
            ->paginate($perPage);
    }
}
