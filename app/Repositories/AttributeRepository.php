<?php

namespace App\Repositories;

use App\Models\Attribute as Model;

/**
 * Class AttributeRepository
 * @package App\Repositories
 */
class AttributeRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass(): string
    {
        return Model::class;
    }

    /**
     * @param int $categoryId
     * @param int|null $perPage
     * @return mixed
     */
    public function getByCategoryIdWithPaginate(int $categoryId, $perPage = null)
    {
        $columns = ['id', 'name', 'type', 'postfix'];

        return $this
            ->startConditions()
            ->select($columns)
            ->where('category_id', $categoryId)
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * @param int $categoryId
     * @param int $product_id
     * @return mixed
     */
    public function getByCategoryIdWithProductValues(int $categoryId, int $product_id)
    {
        $columns = ['id', 'name', 'type', 'type', 'postfix'];

        return $this
            ->startConditions()
            ->select($columns)
            ->where('category_id', $categoryId)
            ->with(['value' => function ($query) use ($product_id) {
                $query
                    ->where('product_id', $product_id)
                    ->select(['id', 'product_id', 'attribute_id', 'value']);
            }])
            ->get();
    }

    /**
     * @param int $id
     * @return Model
     */
    public function firstForEdit(int $id): Model
    {
        return $this
            ->startConditions()
            ->find($id);
    }
}
