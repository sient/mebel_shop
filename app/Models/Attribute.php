<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * Class Attribute
 * @package App\Models
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property int $type
 * @property string|null $postfix
 */
class Attribute extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'name',
        'type',
        'postfix'
    ];

    /**
     * @var array
     */
    public const TYPES = [
        1   => 'string',
        2   => 'integer',
        3   => 'bool'
    ];

    /**
     * @var array
     */
    public const OPTION_TYPES = [
        1   => 'Строка',
        2   => 'Число',
        3   => 'Да/Нет'
    ];

    /**
     * @return string
     */
    public function getType(): string
    {
        return self::TYPES[$this->type];
    }

    /**
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return HasMany
     */
    public function values(): HasMany
    {
        return $this->hasMany(ProductAttribute::class);
    }

    /**
     * @return HasOne
     */
    public function value(): HasOne
    {
        return $this->hasOne(ProductAttribute::class);
    }
}
