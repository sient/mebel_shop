<?php

namespace App\Http\Requests\Web\Admin\CategoryAttribute;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|string|min:2|max:255',
            'type'      => 'required|integer|in:1,2,3',
            'postfix'   => 'nullable|string|min:1|max:255'
        ];
    }
}
