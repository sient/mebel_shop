<?php

namespace App\Http\Requests\Web\Admin\Product;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'          => 'required|string|min:2|max:255',
            'category_id'   => 'required|integer|exists:categories,id',
            'article'       => 'nullable|string|min:1|max:255',
            'brand'         => 'nullable|string|min:2|max:255',
            'manufacturer'  => 'nullable|string|min:2|max:255',
            'guarantee'     => 'nullable|integer|min:1|max:9999',
            'life_time'     => 'nullable|integer|min:1|max:9999',
            'price'         => 'required|numeric|min:0|max:9999999',
            'images'        => 'nullable|array|max:6',
            'images.*'      => 'nullable|image'
        ];
    }
}
