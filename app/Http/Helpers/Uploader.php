<?php

namespace App\Http\Helpers;

use Illuminate\Support\Str;

class Uploader
{
    /**
     * @param $path
     * @param $file
     * @return string
     */
    public static function upload($path, $file): string
    {
        $name = Str::random(32) . '.' . $file->getClientOriginalExtension();
        $file->move($path, $name);

        $protocol = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https://' : 'http://';

        return $protocol . $_SERVER['HTTP_HOST'] . '/' . $path . '/' . $name;
    }
}
