<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\ProductAttribute;
use App\Repositories\AttributeRepository;
use App\Repositories\ProductRepository;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class ProductAttributeController extends Controller
{
    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CategoryAttributeController constructor.
     */
    public function __construct()
    {
        $this->productRepository = app(ProductRepository::class);
        $this->attributeRepository = app(AttributeRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $product_id
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(int $product_id)
    {
        $product = $this->productRepository
            ->firstForEdit($product_id);

        if (empty($product)) {
            return back()
                ->with('error', 'Товар не найден');
        }

        $attributes = $this->attributeRepository
            ->getByCategoryIdWithProductValues($product->category_id, $product->id);

        Breadcrumbs::setCurrentRoute('admin.products.attributes.index', $product->category);

        return view('admin.product-attributes.create', [
            'attributes'    => $attributes,
            'title'         => 'Атрибуты для ' . $product->name
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param  int  $product_id
     * @return Application|Factory|View|RedirectResponse
     */
    public function save(Request $request, int $product_id)
    {
        $product = $this->productRepository
            ->firstForEdit($product_id);

        if (empty($product)) {
            return back()
                ->with('error', 'Товар не найден');
        }

        $data = $request->all();

        $attributes = $this->attributeRepository
            ->getByCategoryIdWithProductValues($product->category_id, $product->id);

        foreach ($attributes as $attribute) {
            if (array_key_exists($attribute->id, $data)) {
                $attributeData = [
                    'attribute_id'  => $attribute->id,
                    'product_id'    => $product->id,
                    'value'         => $data[$attribute->id]
                ];

                $exists = ProductAttribute::where([
                    ['attribute_id', $attribute->id],
                    ['product_id', $product->id]
                ])->first();

                if ($exists) {
                    if ($data[$attribute->id] == null) {
                        $exists->delete();
                    } else {
                        $exists->update($attributeData);
                    }
                } else {
                    if ($data[$attribute->id]) {
                        ProductAttribute::create($attributeData);
                    }
                }
            }
        }

        return redirect()
            ->route('admin.products.attributes.index', $product->id)
            ->with('success', 'Изменения сохранены');
    }
}
