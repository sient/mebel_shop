<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\Category\CreateRequest;
use App\Http\Requests\Web\Admin\Category\UpdateRequest;
use App\Models\Category;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * CategoryController constructor.
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->productRepository = app(ProductRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index()
    {
        $categories = $this->categoryRepository
            ->getAllWithPaginate(10);

        return view('admin.categories.index', [
            'categories'    => $categories,
            'title'         => 'Каталог'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.categories.create', [
            'title' => 'Добавление категории'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRequest  $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $data = $request->validated();

        Category::create($data);

        return redirect()
            ->route('admin.categories.index')
            ->with('success', 'Категория добавлена');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function show(int $id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $products = $this->productRepository
            ->getByCategoryId($category->id, 10);

        Breadcrumbs::setCurrentRoute('admin.categories.show', $category->name);

        return view('admin.categories.show', [
            'category'  => $category,
            'products'  => $products,
            'title'     => 'Категория ' . $category->name
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(int $id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        return view('admin.categories.edit', [
            'category'  => $category,
            'title'     => 'Редактирование категории'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRequest  $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, int $id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $data = $request->validated();
        $category->update($data);

        return redirect()
            ->route('admin.categories.index')
            ->with('success', 'Изменения сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
