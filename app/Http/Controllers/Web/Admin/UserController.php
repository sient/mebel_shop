<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\User\UpdateRequest;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

/**
 * Class UserController
 * @package App\Http\Controllers\Web\Admin
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * UserController constructor.
     */
    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
    }

    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = $this->userRepository
            ->getAllWithPaginate(10);

        return view('admin.users.index', [
            'users' => $users,
            'title' => 'Список пользователей'
        ]);
    }

    /**
     * @param int $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(int $id)
    {
        $user = $this->userRepository->firstForEdit($id);

        if (empty($user)) {
            return redirect()
                ->route('admin.users.index')
                ->with('error', 'Пользователь не найден');
        }

        return view('admin.users.edit', [
            'user'  => $user,
            'roles' => User::ROLES,
            'title' => 'Редактирование пользователя'
        ]);
    }

    /**
     * @param UpdateRequest $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, int $id)
    {
        $user = $this->userRepository->firstForEdit($id);

        if (empty($user)) {
            return redirect()
                ->route('admin.users.index')
                ->with('error', 'Пользователь не найден');
        }

        $data = $request->validated();

        $user->update($data);

        return redirect()
            ->route('admin.users.index')
            ->with('success', 'Изменения сохранены');
    }
}
