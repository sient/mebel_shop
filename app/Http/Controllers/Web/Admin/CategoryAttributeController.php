<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\Admin\CategoryAttribute\CreateRequest;
use App\Models\Attribute;
use App\Repositories\AttributeRepository;
use App\Repositories\CategoryRepository;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CategoryAttributeController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var AttributeRepository
     */
    private $attributeRepository;

    /**
     * CategoryAttributeController constructor.
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->attributeRepository = app(AttributeRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @param int $category_id
     * @return Application|Factory|View|RedirectResponse
     */
    public function index(int $category_id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($category_id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $attributes = $this->attributeRepository
            ->getByCategoryIdWithPaginate($category_id, 10);

        Breadcrumbs::setCurrentRoute('admin.categories.attributes.index', $category);

        return view('admin.category-attributes.index', [
            'attributes'    => $attributes,
            'category'      => $category,
            'title'         => 'Атрибуты ' . $category->name
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $category_id
     * @return Application|Factory|View|RedirectResponse
     */
    public function create(int $category_id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($category_id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        Breadcrumbs::setCurrentRoute('admin.categories.attributes.create', $category);

        return view('admin.category-attributes.create', [
            'title' => 'Добавление аттрибута',
            'types' => Attribute::OPTION_TYPES
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @param int $category_id
     * @return Application|Factory|View|RedirectResponse
     */
    public function store(CreateRequest $request, int $category_id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($category_id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $data = $request->validated();
        $data['category_id'] = $category->id;

        Attribute::create($data);

        return redirect()
            ->route('admin.categories.attributes.index', $category->id)
            ->with('success', 'Аттрибут добавлен');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $category_id
     * @param  int  $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit($category_id, $id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($category_id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $attribute = $this->attributeRepository
            ->firstForEdit($id);

        Breadcrumbs::setCurrentRoute('admin.categories.attributes.edit', $category);

        return view('admin.category-attributes.edit', [
            'attribute' => $attribute,
            'types'     => Attribute::OPTION_TYPES,
            'title'     => 'Редактирование аттрибута'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CreateRequest $request
     * @param  int  $category_id
     * @param  int  $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function update(CreateRequest $request, int $category_id, int $id)
    {
        $category = $this->categoryRepository
            ->firstForEdit($category_id);

        if (empty($category)) {
            return redirect()
                ->route('admin.categories.index')
                ->with('error', 'Категория не найдена');
        }

        $attribute = $this->attributeRepository
            ->firstForEdit($id);

        $data = $request->validated();
        $attribute->update($data);

        return redirect()
            ->route('admin.categories.attributes.index', $category->id)
            ->with('success', 'Изменения сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
