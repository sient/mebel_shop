<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * @return Application|Factory|View
     */
    public function index()
    {
        $users = [
            'total' => $this->usersTotal(),
            'today' => $this->usersToday()
        ];

        $users['percent'] = intval($users['today'] / $users['total'] * 100);

        return view('admin.index', [
            'users' => $users
        ]);
    }

    private function usersTotal()
    {
        return User::count();
    }

    private function usersToday()
    {
        return User::whereDate('created_at', Carbon::today())
            ->count();
    }
}
