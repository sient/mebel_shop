<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Helpers\Uploader;
use App\Http\Requests\Web\Admin\Product\CreateRequest;
use App\Http\Requests\Web\Admin\Product\UpdateRequest;
use App\Models\Product;
use App\Models\ProductImage;
use App\Repositories\CategoryRepository;
use App\Repositories\ProductRepository;
use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ProductController constructor.
     */
    public function __construct()
    {
        $this->categoryRepository = app(CategoryRepository::class);
        $this->productRepository = app(ProductRepository::class);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        $categories = $this->categoryRepository
            ->getForSelect();

        return view('admin.products.create', [
            'categories'    => $categories,
            'title'         => 'Добавление товара'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return RedirectResponse
     */
    public function store(CreateRequest $request)
    {
        $data = $request->validated();

        $images = [];
        foreach ($data['images'] as $image) {
            $images[]['image'] = Uploader::upload('products/images', $image);
        }

        $product = Product::create($data);
        $product->images()->createMany($images);

        return redirect()
            ->route('admin.categories.show', $product->category_id)
            ->with('success', 'Товар успешно добавлен');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Application|Factory|View|RedirectResponse
     */
    public function edit(int $id)
    {
        $product = $this->productRepository
            ->firstForEdit($id);

        if (empty($product)) {
            return back()
                ->with('error', 'Товар не найден');
        }

        $categories = $this->categoryRepository
            ->getForSelect();

        Breadcrumbs::setCurrentRoute('admin.products.edit', $product->category);

        return view('admin.products.edit', [
            'product'       => $product,
            'categories'    => $categories,
            'title'         => 'Редактирование товара'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param  int  $id
     * @return RedirectResponse
     */
    public function update(UpdateRequest $request, $id)
    {
        $product = $this->productRepository
            ->firstForEdit($id);

        if (empty($product)) {
            return back()
                ->with('error', 'Товар не найден');
        }

        $data = $request->validated();

        $product->update($data);

        if ($request->has('images')) {
            $images = [];
            foreach ($data['images'] as $image) {
                $images[]['image'] = Uploader::upload('products/images', $image);
            }

            $product->images()->createMany($images);
        }

        return redirect()
            ->route('admin.products.edit', $product->id)
            ->with('success', 'Изменения сохранены');
    }

    /**
     * @param int $id
     * @return RedirectResponse
     */
    public function deleteImage(int $id)
    {
        $image = ProductImage::where('id', $id)
            ->firstOrFail();

        $product_id = $image->product_id;

        $image->delete();

        return redirect()
            ->route('admin.products.edit', $product_id)
            ->with('success', 'Изображение удалено');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
