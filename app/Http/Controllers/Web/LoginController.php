<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\LoginRequest;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $data = $request->validated();

        $user = User::where('email', $data['email'])
            ->first();

        if (!$user) {
            return redirect()
                ->route('home')
                ->withInput()
                ->withErrors([
                    'email' => 'Пользователь не найден'
                ]);
        }

        if ($user->role != 3) {
            return redirect()
                ->route('home')
                ->withInput()
                ->withErrors([
                    'email' => 'Пользователь не найден'
                ]);
        }

        if (!Hash::check($data['password'], $user->password)) {
            return redirect()
                ->route('home')
                ->withInput()
                ->withErrors([
                    'password' => 'Неверный пароль'
                ]);
        }

        Auth::login($user);

        return redirect()
            ->route('admin.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::guard()->logout();

        Session::flush();
        Session::regenerate();

        return redirect()
            ->route('home');
    }
}
