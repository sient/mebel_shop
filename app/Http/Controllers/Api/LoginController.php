<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

/**
 * Class LoginController
 * @package App\Http\Controllers\Api
 */
class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
        $this->middleware('guest')
            ->except('logout');
    }

    /**
     * @param LoginRequest $request
     * @return JsonResponse
     */
    public function login(LoginRequest $request): JsonResponse
    {
        $data = $request->validated();

        $user = User::where('email', $data['email'])
            ->first();

        if (!Hash::check($data['password'], $user->password)) {
            return $this->error('Неверный пароль', 401);
        }

        Auth::login($user);

        return $this->success([
            'token' => $user->createToken('api')->accessToken
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function logout(Request $request): JsonResponse
    {
        if (!$this->api_guard()->check()) {
            return $this->error('Active user session not founded');
        }

        $request->user('api')->token()->revoke();

        Auth::guard()->logout();

        Session::flush();
        Session::regenerate();

        return $this->success('Success logout');
    }

    /**
     * @return Guard|StatefulGuard
     */
    public function api_guard()
    {
        return Auth::guard('api');
    }
}
